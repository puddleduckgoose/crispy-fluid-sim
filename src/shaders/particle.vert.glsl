#version 430
layout(location = 0) in vec2 position;

struct ParticleState
{
    float position[2];
    float padding[2];
};

layout(std430, binding=2) buffer particleStateLayout
{
    ParticleState particleStates[];
};

mat4 Orthographic(
    float left, float right, float bottom, float top, float zNear,
    float zFar)
{
    mat4 result = mat4(1.0);

    result[0][0] = 2.0f / (right - left);
    result[1][1] = 2.0f / (top - bottom);
    result[2][2] = -2.0f / (zFar - zNear);
    result[3][0] = -(right + left) / (right - left);
    result[3][1] = -(top + bottom) / (top - bottom);
    result[3][2] = -(zFar + zNear) / (zFar - zNear);
    result[3][3] = 1.0f;

    return result;
}

mat4 Translate(vec3 translation)
{
    mat4 result = mat4(1.0);
    result[3][0] = translation.x;
    result[3][1] = translation.y;
    result[3][2] = translation.z;

    return result;
}

mat4 Scale(float scale)
{
    mat4 result = mat4(scale);
    result[3][3] = 1.0f;
    return result;
}

void main() {
    vec2 particlePosition = vec2(particleStates[gl_InstanceID].position[0],
        particleStates[gl_InstanceID].position[1]);

    mat4 projection = Orthographic(0, 1024, 0, 768, -0.5, 0.5);
    mat4 model = Translate(vec3(particlePosition, 0.0)) * Scale(20.0);
    gl_Position = projection * model * vec4(position, 0.0, 1.0);
}
