#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

static const void *ReadEntireFileIntoMemory(const char *path) {
    FILE *file = fopen(path, "r");
    assert(file != NULL);

    fseek(file, 0, SEEK_END);
    uint32_t length = ftell(file);
    rewind(file);

    void *data = calloc(length + 1, 1);
    fread(data, 1, length, file);

    return data;
}

static void GLAPIENTRY OpenGLMessageCallback(GLenum source, GLenum type,
                                             GLuint id, GLenum severity,
                                             GLsizei length,
                                             const GLchar *message,
                                             const void *userParam) {
    printf("OPENGL MESSAGE: %s type = 0x%x, severity = 0x%x, message = %s\n",
           (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""), type,
           severity, message);
}

typedef struct {
    float position[2];
    float velocity[2];
} ParticleState;

typedef struct {
    float min[2];
    float max[2];
} Box;

typedef struct {
    Box boxes[8];
    uint32_t count;
} CollisionWorld;

static bool IsColliding(float x, float y, Box box) {
    return (x > box.min[0] && x < box.max[0] && y > box.min[1] &&
            y < box.max[1]);
}

static Box CreateBox(float x0, float y0, float x1, float y1) {
    Box box;
    box.min[0] = x0;
    box.min[1] = y0;
    box.max[0] = x1;
    box.max[1] = y1;
    return box;
}

static bool IsCollidingWithWorld(float x, float y,
                                 CollisionWorld *collisionWorld) {
    bool result = false;
    for (uint32_t index = 0; index < collisionWorld->count; index++) {
        if (IsColliding(x, y, collisionWorld->boxes[index])) {
            result = true;
            break;
        }
    }

    return result;
}

static void InitializeParticles(ParticleState *particleState,
                                uint32_t particleCount) {
    for (uint32_t i = 0; i < particleCount; i++) {
        particleState[i].position[0] = 30.0f * i;
        particleState[i].position[1] = 600.0f;

        particleState[i].velocity[0] = 0.0f;
        particleState[i].velocity[1] = 0.0f;
    }
}

static void UpdateParticles(ParticleState *particleState,
                            uint32_t particleCount,
                            CollisionWorld *collisionWorld, float dt) {
    float gravity = 20.0f;

    for (uint32_t particleIndex = 0; particleIndex < particleCount;
         particleIndex++) {
        particleState[particleIndex].velocity[1] += -gravity * dt;
        particleState[particleIndex].position[1] +=
            particleState[particleIndex].velocity[1] * dt;

        if (IsCollidingWithWorld(particleState[particleIndex].position[0],
                                 particleState[particleIndex].position[1],
                                 collisionWorld)) {
            particleState[particleIndex].velocity[1] =
                -0.95f * particleState[particleIndex].velocity[1];
        }
    }
}

int main(int argc, char **argv) {
    printf("Hello, World!\n");

    if (!glfwInit()) {
        return 1;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow *window = glfwCreateWindow(1024, 768, "Hello World", NULL, NULL);
    if (window == NULL) {
        glfwTerminate();
        return 1;
    }
    glfwMakeContextCurrent(window);

    GLenum glewError = glewInit();
    if (glewError != GLEW_OK) {
        fprintf(stderr, "GLEW error: %s\n", glewGetErrorString(glewError));
        glfwTerminate();
        return 1;
    }

    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(OpenGLMessageCallback, 0);

    // Enable all debug messages
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL,
                          GL_TRUE);

    printf("GL_VENDOR: %s\n", (const char *)glGetString(GL_VENDOR));
    printf("GL_RENDERER: %s\n", (const char *)glGetString(GL_RENDERER));
    printf("GL_VERSION: %s\n", (const char *)glGetString(GL_VERSION));
    printf("GL_SHADING_LANGUAGE_VERSION: %s\n",
           (const char *)glGetString(GL_SHADING_LANGUAGE_VERSION));

    float vertices[] = {-0.5f, -0.5f, 0.5f, -0.5f, 0.5f,  0.5f,
                        -0.5f, -0.5f, 0.5f, 0.5f,  -0.5f, 0.5f};

    uint32_t vertexBuffer;
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // Create vertex array object
    uint32_t vertexArrayObject;
    glGenVertexArrays(1, &vertexArrayObject);
    glBindVertexArray(vertexArrayObject);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, NULL);

    uint32_t particleCount = 16;
    uint32_t particleStateBuffer;
    glGenBuffers(1, &particleStateBuffer);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, particleStateBuffer);
    glBufferStorage(
        GL_SHADER_STORAGE_BUFFER, sizeof(ParticleState) * particleCount, NULL,
        GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
    ParticleState *particleState =
        (ParticleState *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);

    // TODO: Cleanup duplication
    uint32_t vertexShader = glCreateShader(GL_VERTEX_SHADER);
    uint32_t fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

    const char *vertexShaderCode = (const char *)ReadEntireFileIntoMemory(
        "src/shaders/particle.vert.glsl");
    const char *fragmentShaderCode = (const char *)ReadEntireFileIntoMemory(
        "src/shaders/particle.frag.glsl");

    glShaderSource(vertexShader, 1, (const char **)&vertexShaderCode, NULL);
    glShaderSource(fragmentShader, 1, &fragmentShaderCode, NULL);

    glCompileShader(vertexShader);
    glCompileShader(fragmentShader);

    int32_t status = 0;
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);

    if (!status) {
        char log[256];
        glGetShaderInfoLog(vertexShader, sizeof(log), NULL, log);
        printf("%s\n", vertexShaderCode);
        fprintf(stderr, "Failed to compile shaders: \n%s\n", log);
    }

    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);
    if (!status) {
        char log[256];
        glGetShaderInfoLog(fragmentShader, sizeof(log), NULL, log);
        printf("%s\n", fragmentShaderCode);
        fprintf(stderr, "Failed to compile shaders: \n%s\n", log);
    }

    uint32_t program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (!status) {
        char log[256];
        glGetProgramInfoLog(program, sizeof(log), NULL, log);
        fprintf(stderr, "Failed to link program:\n%s\n", log);
    }

    glUseProgram(program);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, particleStateBuffer);

    InitializeParticles(particleState, particleCount);

    CollisionWorld collisionWorld = {};
    collisionWorld.boxes[0] = CreateBox(80.0f, 80.0f, 700.0f, 130.0f);
    collisionWorld.count = 1;

    double currentTime = glfwGetTime();
    while (!glfwWindowShouldClose(window)) {
        double frameTime = glfwGetTime() - currentTime;
        currentTime = glfwGetTime();
        glfwPollEvents();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glClearColor(0.15, 0.05, 0.0, 1.0);

        float dt = frameTime;
        UpdateParticles(particleState, particleCount, &collisionWorld, dt);

        glDrawArraysInstanced(GL_TRIANGLES, 0, 6, particleCount);

        glfwSwapBuffers(window);
    }

    glfwTerminate();
    return 0;
}
