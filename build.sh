#!/bin/sh

glfw3_libs=$(pkg-config --libs glfw3)
glew_libs=$(pkg-config --libs glew)

gcc src/main.c -o build/main -std=gnu99 $glfw3_libs $glew_libs
